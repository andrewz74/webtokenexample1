# Webtokenexample1
In Visual Studio right mouse on Solution name and select **Set Startup Project** to run both projects.

In browser open this URL to get TOKEN:

<http://localhost:40200/api/token>

To test API open URL:

<http://localhost/40201/api/public>

To test private API, add previous token to request (example with curl):

    curl -i -H "Accept: application/json" -H "Authorization: Bearer [TOKEN]" http://localhost:40201/api/private

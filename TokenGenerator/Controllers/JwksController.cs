﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TokenGenerator.Controllers
{
	[Route(".well-known/jwks.json")]
	[ApiController]
	public class JwksController : ControllerBase
	{
		private readonly IHelper _helper;

		public JwksController(IHelper helper)
		{
			_helper = helper ?? throw new ArgumentNullException(nameof(helper));
		}

		[EnableCors("mycors")]
		public async Task<IActionResult> GetAsync()
		{
			var certificateInfo = await _helper.GetCertificateInfoFromDiskAsync();

			var obj = new KeyJwtClass
			{
				Keys = new[]
				{
					new JwksClass
					{
						Alg = "RS256",
						Kty = "RSA",
						Use = "sig",
						N = certificateInfo.Modulus,
						E = "AQAB",
						Kid= certificateInfo.KeyId,
						X5t = certificateInfo.X5t,
						X5c = new [] { certificateInfo.PublicCertificateBase64 }
					}
				}
			};

			return Ok(obj);
		}
	}

	public class KeyJwtClass
	{
		[JsonPropertyName("keys"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public JwksClass[] Keys { get; set; }
	}

	public class JwksClass
	{
		[JsonPropertyName("alg"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string Alg { get; set; }
		[JsonPropertyName("kty"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string Kty { get; set; }
		[JsonPropertyName("use"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string Use { get; set; }
		[JsonPropertyName("n"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string N { get; set; }
		[JsonPropertyName("e"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string E { get; set; }
		[JsonPropertyName("kid"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string Kid { get; set; }
		[JsonPropertyName("x5t"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string X5t { get; set; }
		[JsonPropertyName("x5c"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] X5c { get; set; }
	}
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace TokenGenerator.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TokenController : ControllerBase
	{
		private readonly IHelper _helper;

		public TokenController(IHelper helper)
		{
			_helper = helper ?? throw new ArgumentNullException(nameof(helper)); ;
		}

		public async Task<IActionResult> Index()
		{
			var token = await _helper.GetTokenAsync();
			return Ok(token);
		}
	}
}

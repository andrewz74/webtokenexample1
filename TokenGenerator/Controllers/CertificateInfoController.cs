﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace TokenGenerator.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CertificateInfoController : ControllerBase
	{
		private readonly IHelper _helper;

		public CertificateInfoController(IHelper helper)
		{
			_helper = helper ?? throw new ArgumentNullException(nameof(helper)); ;
		}

		public async Task<IActionResult> Index()
		{
			var ci = await _helper.GetCertificateInfoFromDiskAsync();
			return Ok(ci);
		}
	}
}

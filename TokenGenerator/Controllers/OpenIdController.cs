﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json.Serialization;

namespace TokenGenerator.Controllers
{
    [Route(".well-known/openid-configuration")]
	[ApiController]
	public class OpenIdController : ControllerBase
	{
		private readonly Configuration _configuration;

		public OpenIdController(Configuration configuration)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}

		[EnableCors("mycors")]
		public IActionResult Get()
		{
			var obj = new OpenIdConfiguration
			{
				Issuer = _configuration.Issuer,
				AuthorizationEndpoint = null,
				TokenEndpoint = null,
				DeviceAuthorizationEndpoint = null,
				UserinfoEndpoint = null,
				MfaChallengeEndpoint = null,
				JwksUri = _configuration.Issuer + ".well-known/jwks.json",
				RegistrationEndpoint = null,
				RevocationEndpoint = null,
				ScopesSupported = new[] { "openid", "profile" },
				ResponseTypesSupported = new[] { "code", "token", "code token" },
				CodeChallengeMethodsSupported = new[] { "S256", "plain" },
				ResponseModesSupported = new[] { "query", "fragment", "form_post" },
				SubjectTypesSupported = new[] { "public" },
				IdTokenSigningAlgValuesSupported = new[] { "RS256" },
				TokenEndpointAuthMethodsSupported = new[] { "client_secret_post" },
				ClaimsSupported = new[] { "aud", "auth_time", "created_at", "email", "email_verified", "exp", "family_name", "given_name", "iat", "identities", "iss", "name", "nickname", "phone_number", "picture", "sub" },
				RequestUriParameterSupported = false
			};

			return Ok(obj);
		}
	}

	public class OpenIdConfiguration
	{
		[JsonPropertyName("issuer"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string Issuer { get; set; }
		[JsonPropertyName("authorization_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string AuthorizationEndpoint { get; set; }
		[JsonPropertyName("token_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string TokenEndpoint { get; set; }
		[JsonPropertyName("device_authorization_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] DeviceAuthorizationEndpoint { get; set; }
		[JsonPropertyName("userinfo_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string UserinfoEndpoint { get; set; }
		[JsonPropertyName("mfa_challenge_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string MfaChallengeEndpoint { get; set; }
		[JsonPropertyName("jwks_uri"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string JwksUri { get; set; }
		[JsonPropertyName("registration_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string RegistrationEndpoint { get; set; }
		[JsonPropertyName("revocation_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string RevocationEndpoint { get; set; }
		[JsonPropertyName("scopes_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] ScopesSupported { get; set; }
		[JsonPropertyName("response_types_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] ResponseTypesSupported { get; set; }
		[JsonPropertyName("code_challenge_methods_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] CodeChallengeMethodsSupported { get; set; }
		[JsonPropertyName("response_modes_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] ResponseModesSupported { get; set; }
		[JsonPropertyName("subject_types_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] SubjectTypesSupported { get; set; }
		[JsonPropertyName("id_token_signing_alg_values_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] IdTokenSigningAlgValuesSupported { get; set; }
		[JsonPropertyName("token_endpoint_auth_methods_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] TokenEndpointAuthMethodsSupported { get; set; }
		[JsonPropertyName("claims_supported"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[] ClaimsSupported { get; set; }
		[JsonPropertyName("request_uri_parameter_supported")]
		public bool RequestUriParameterSupported { get; set; }
	}
}

﻿namespace TokenGenerator
{
	public class Configuration
	{
		public string PathData { get; set; }
		public string FileNameCertificateInfo { get; set; }
		public int RsaKeySize { get; set; }
		public string PfxPassword { get; set; }
		public string Issuer { get; set; }
		public string Audience { get; set; }
	}
}

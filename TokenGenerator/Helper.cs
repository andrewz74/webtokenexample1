﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TokenGenerator
{
    public interface IHelper
	{
		Task<CertificateInfo> GetCertificateInfoFromDiskAsync();
		Task<string> GetTokenAsync();
	}

	public class Helper : IHelper
	{
		private readonly Configuration _configuration;
		private readonly ICertificateHelper _certificateHelper;
		private readonly ITokenHelper _tokenHelper;

		private Helper() => throw new NotImplementedException();
		public Helper(Configuration configuration, ICertificateHelper certificateHelper, ITokenHelper tokenHelper)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
			_certificateHelper = certificateHelper ?? throw new ArgumentNullException(nameof(certificateHelper));
			_tokenHelper = tokenHelper ?? throw new ArgumentNullException(nameof(tokenHelper));
		}
		public async Task<CertificateInfo> GetCertificateInfoFromDiskAsync()
		{
			CertificateInfo certificate;
			string filename = Path.Combine(_configuration.PathData, _configuration.FileNameCertificateInfo);
			if (!File.Exists(filename))
			{
				// create new certificate
				certificate = _certificateHelper.GenerateCertificate();
				CheckDirectoryDestination(_configuration.PathData);
				var jsonString = JsonConvert.SerializeObject(certificate);
				await File.WriteAllTextAsync(filename, jsonString);
			}
			else
			{
				string jsonString = await File.ReadAllTextAsync(filename);
				certificate = JsonConvert.DeserializeObject<CertificateInfo>(jsonString);
			}

			return certificate;
		}

		public async Task<string> GetTokenAsync()
		{
			return _tokenHelper.GenerateToken(await GetCertificateInfoFromDiskAsync());
		}

		private void CheckDirectoryDestination(in string pathData)
		{
			var di = new DirectoryInfo(pathData);
			if (!di.Exists)
			{
				di.Create();
			}
		}
	}
}

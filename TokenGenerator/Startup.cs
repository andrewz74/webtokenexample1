using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace TokenGenerator
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			var configuration = Configuration.GetSection("Configuration").Get<Configuration>();

			services.AddCors(options =>
			{
				options.AddPolicy(name: "mycors", builder => builder
					.AllowAnyMethod()
					.AllowAnyOrigin()
					.AllowAnyHeader()
				);
			});

			services.AddControllers();
			services.AddSingleton(configuration);
			services.AddSingleton<ICertificateHelper, CertificateHelper>();
			services.AddSingleton<ITokenHelper, TokenHelper>();
			services.AddSingleton<IHelper, Helper>();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseRouting();
			app.UseCors();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}

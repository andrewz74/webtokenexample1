﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;

namespace TokenGenerator
{
    public interface ITokenHelper
	{
		string GenerateToken(in CertificateInfo ci);
	}

	public class TokenHelper : ITokenHelper
	{
		private readonly Configuration _configuration;

		private TokenHelper() => throw new NotImplementedException();
		public TokenHelper(Configuration configuration)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}

		public string GenerateToken(in CertificateInfo ci)
		{
			var certificate = new X509Certificate2(ci.Pfx, _configuration.PfxPassword);
			var securityKey = new X509SecurityKey(certificate);

			var claims = new[]
		    {
				new Claim(JwtRegisteredClaimNames.Email, "ax2@ax2.it"),
				new Claim(ClaimTypes.NameIdentifier, "xa"),
				new Claim("scope", "openid profile read:messages")
			};

			var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.RsaSha256);
			var token = new JwtSecurityToken(
				_configuration.Issuer,
				_configuration.Audience,
				claims,
				notBefore: DateTime.Now,
				expires: DateTime.Now.AddMinutes(5),
				signingCredentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
	}
}
